# My Bonuses 
In this project I have added some bonuses

## Docker
To make the installation easier I have set up a __docker-compose.yml__ with three __DockerFiles__ that are in the "apache", "composer" and "php" folder.

## A bit of CSS, bootstrap
To make the page look better I have used bootstrap to style the list of commits and the form. But I have not style the details because of the time it takes to do something beautiful.

## Choose the repo
I have implemented the feature that allow you to choose the repo you want. 

## Composer
Finally I have set up composer in the docker environement to use the Guzzle package. 