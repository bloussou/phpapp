<?php $title = 'GitHub View'; ?>
<?php ob_start(); // Waiting for the data?>
<table>
    <?php
    foreach($list as $v){
    ?>
        <tr>
            <th>
                <a href="<?= $v['author']['html_url'] ?>">
                    <img src="<?= $v['author']['avatar_url'] ?>"  style='width:60px;height:60px;border-rad' />
                </a>
            </th>
            <th>
                <a href="index.php?url=<?= $v['commit']['url'] ?>" class='list-group-item list-group-item-action flex-column align-items-start'>
                    <div class='d-flex w-100 justify-content-between'>
                        <h5 class='mb-1'><?= htmlspecialchars($v['commit']['author']['name']) ?></h5>
                        <small class='text-muted'><?= htmlspecialchars($v['commit']['author']['date']) ?></small>
                    </div>
                    <p class='mb-1'><?= htmlspecialchars($v['commit']['message']) ?></p>
                    <small class='text-muted'>id : <?= htmlspecialchars($v['sha']) ?></small>
                </a>
            </th>
        </tr>
    <?php
    }
    ?>
</table>
<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>
