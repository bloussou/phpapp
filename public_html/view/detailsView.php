<link rel="stylesheet" type="text/css" href="./style.css" >
<?php $title = 'Commit Details'; ?>
<?php ob_start(); // Waiting for the data?>

<div class='page'>
    <div class= 'titles'>
        <div class= 'title'>id : <?=$commit['sha']?></div>
        <div class= 'title'><?=$commit['author']['date']?></div>
        <div class= 'title'>
            <a href="<?=$commit['html_url']?>">view on git</a>
        </div>
    </div>
    <h2 >Message : </h2>
    <div ><?=$commit['message']?></div>
    <div class='parents'>
        <div>Parents : </div>
        <div class='parent'>
            <?php
            foreach ($commit['parents'] as $v) 
            {
            ?>
                <div>
                    <a href="details.php?url=<?=$v['url']?>"><?=$v['sha']?></a>
                </div>
            <?php
            }
            ?>
        </div>
    </div> 
    <div class='footer'>
        <div>author : <?=$commit['author']['name']?></div>
        <div>email : <?=$commit['author']['email']?></div>
    </div>
</div>

<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>