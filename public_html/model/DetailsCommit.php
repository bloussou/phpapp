<?php
require 'vendor/autoload.php';

class DetailsCommit
{
    /*
    Get details return the result to the api github with this type of url :
    "https://api.github.com/repos/fd0r/react-personnal-page/git/commits/1d4db39a3fbea8bb613f1cde1ba3b9e38a221032"
    */
    function getDetails($url) 
    {
        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $url);
            $body = $res->getBody();
            return json_decode($body, true);
        } catch (Exception $e) {
            die('Error : '.$e->getMessage());
        }
    }
}