<?php

require_once('model/ListCommits.php');
require_once('model/DetailsCommit.php');
/*
listCommits() can take two arguments. Then it intantiate the model
ListCommits and do the request. Finally it set up the listView view.
*/
function listCommits($owner = "torvalds", $repos = "linux")
{
    $listCommits = new ListCommits(); 
    $list = $listCommits->getCommits($owner, $repos); 

    require('view/listView.php');
}
/*
detailsCommit() can take two arguments. Then it intantiate the model
DetailsCommit and do the request. Finally it set up the detailsView view.
*/
function detailsCommit($url)
{
    $detailsCommit = new DetailsCommit(); 
    $commit = $detailsCommit->getDetails($url); 

    require('view/detailsView.php');
}
/*
displayForm() just display the formView view.
*/
function displayForm()
{
    require('view/formView.php');
}